
package ru.nsu.g.kigel.m.phrases;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.omg.CORBA_2_3.portable.InputStream;
import org.omg.CORBA_2_3.portable.OutputStream;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.*;

public class Phrases {

    private Integer N;
    private Integer M;
    private String filename;
    private HashMap<String, Integer> freq;
    private InputStream input;

    Phrases() {
        freq = new HashMap<String, Integer>();
        N = 2;
        M = 2;
        input = (InputStream) System.in;
        filename = "-";
    }

    Phrases(Integer N, Integer M) {
        freq = new HashMap<String, Integer>();
        this.M = M;
        this.N = N;
        filename = "-";
        input = (InputStream) System.in;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setInput(InputStream input) {
        this.input = input;
    }

    public void setM(Integer m) {
        this.M = m;
    }

    public void setN(Integer n) {
        this.N = n;
    }

    public void counter() {
        Scanner scanner;
        scanner = new Scanner(input);
        ArrayList<String> window = new ArrayList<>();
        StringBuilder check = new StringBuilder();

        while (scanner.hasNext()) {
            window.add(scanner.next());
            if (window.size() == N) {
                for (String str : window) {
                    check.append(str);
                    check.append(" ");
                }
                int count = freq.getOrDefault(check.toString().trim(), 0);
                freq.put(check.toString().trim(), count + 1);
                window.remove(0);
                check.setLength(0);
            }
        }
        scanner.close();
    }

    public void print(PrintStream output)
    {
        ArrayList<Map.Entry<String, Integer>> list = new ArrayList<>(freq.entrySet());

        list.sort((e1, e2) -> -(e1.getValue().compareTo(e2.getValue())));

        PrintWriter writer = new PrintWriter(output);

        for (Map.Entry<String, Integer> frequences  : list) {
            if (frequences.getValue() >= M)
                writer.println(frequences.getKey() + " (" + frequences.getValue() + ")");
        }

        writer.flush();
        writer.close();
    }
}