package ru.nsu.g.kigel.m.phrases;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Parse {

    private String [] args;

    public Parse(String [] vvod)
    {
        this.args = vvod;
    }

    int is_ComStr(String Args)
    {
        int position  = Arrays.asList(args).indexOf(Args);
        if(position == -1)
        {
            System.out.println("Can't find this argument");
            return -1;
        }
        if(position != args.length - 1)
        {
            return Integer.parseInt(Arrays.asList(args).get(position + 1));
        }
        else
        {
            System.out.println("Can't find this argument");
            return -1;
        }
    }

    String is_File()
    {
        for(String arg : args)
        {
            if((arg.startsWith("-") && arg.length() == 1) || (!(arg.startsWith("-")) && !arg.matches("\\d+(\\.\\d+)?")))
            {
                return arg;
            }
        }
        return  "filename not found";
    }

}
