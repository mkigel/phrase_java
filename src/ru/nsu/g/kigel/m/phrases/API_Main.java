package ru.nsu.g.kigel.m.phrases;


import java.io.FileNotFoundException;

public class API_Main
{
    public static void main(String [] args) throws FileNotFoundException {
        Parse parser = new Parse(args);
        API_Phrases phrase = new API_Phrases();

        String filename = parser.is_File();
        if(filename.equals("-"))
        {
            phrase.setFilename("-");
        }
        else {
            phrase.setFilename(filename);
        }

        if(parser.is_ComStr("-n") == -1)
        {
            phrase.setN(2);
        }
        else
        {
            phrase.setN(parser.is_ComStr("-n"));
        }

        if(parser.is_ComStr("-m") == -1)
        {
            phrase.setM(2);
        }
        else {
            phrase.setN(parser.is_ComStr("-m"));
        }

        phrase.API_Counter();
        phrase.Print_API(System.out);
    }
}
