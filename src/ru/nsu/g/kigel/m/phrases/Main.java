
package ru.nsu.g.kigel.m.phrases;

public class Main {

    public static void main(String[] args) {

       Parse parser = new Parse (args);
       Phrases phrase = new Phrases();


       String filename = parser.is_File();
       if(filename == "-")
       {
           phrase.setFilename("-");
       }
       else {
           phrase.setFilename(filename);
       }

       if(parser.is_ComStr("-n") == -1)
       {
           phrase.setN(2);
       }
       else
       {
           phrase.setN(parser.is_ComStr("-n"));
       }

       if(parser.is_ComStr("-m") == -1)
       {
           phrase.setM(2);
       }
       else {
               phrase.setN(parser.is_ComStr("-m"));
        }

       phrase.counter();
       phrase.print(System.out);
    }
}
