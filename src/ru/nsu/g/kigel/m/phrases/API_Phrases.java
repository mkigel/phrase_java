package ru.nsu.g.kigel.m.phrases;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class API_Phrases {
    private static Integer M;
    private static Integer N;
    private static String filename;
    private HashMap<String, Integer> freq;
    private InputStream input;


    API_Phrases()
    {
        freq = new HashMap<>();
        filename = "-";
        input = System.in;
    }
    API_Phrases(Integer N, Integer M)
    {
        API_Phrases.M = M;
        API_Phrases.N = N;
        filename = "-";
        input = System.in;
        freq = new HashMap<>();
    }
    static class Accumulator
    {
        private HashMap<String,Integer>  accumulator;
        private ArrayList<String>  buffer;

        Accumulator()
        {
            buffer = new ArrayList<>();
            accumulator = new HashMap<>();
        }

        public String accum(String s1, String s2)
        {
            if(buffer.isEmpty())
            {
                buffer.add(s1);
            }
            buffer.add(s2);
            if(buffer.size() >= N)
            {
                StringBuilder build = new StringBuilder();
                while(buffer.size() >= N)
                {
                    for(int i = 0; i < N; i++)
                    {
                        build.append(buffer.get(i));
                        build.append(" ");
                    }
                    int count = accumulator.getOrDefault(build.toString().trim(), 0);
                    accumulator.put(build.toString().trim(), count + 1);
                    buffer.remove(0);
                    build.setLength(0);
                }
            }
            return  s2;
        }

        public HashMap<String, Integer> getAccumulator()
        {
            return accumulator;
        }

    }



    public void API_Counter() throws FileNotFoundException {
        Stream<String> scan;
        if(!filename.equals("-"))
        {
             BufferedReader in = new BufferedReader(new FileReader(filename));
             scan = in.lines();
        }
        else
        {
            BufferedReader in = new BufferedReader(new InputStreamReader(input));
            scan = in.lines();
        }
        scan = Arrays.stream(scan.collect(Collectors.joining(" ")).split(" "));
        Accumulator a = new Accumulator();
        scan.reduce(a :: accum);
        freq = a.getAccumulator();
    }

    public void Print_API(OutputStream out)
    {
        PrintWriter writer = new PrintWriter(out);
        freq.entrySet().stream().sorted((s1,s2) -> -(s1.getValue().compareTo(s2.getValue())))
                .filter(s -> s.getValue() >= M). forEach(s -> writer.println(s.getKey() + "(" + s.getValue() + ")"));
        writer.flush();
        writer.close();
    }

    public void setN(Integer n) {
        N = n;
    }

    public void setM(Integer m) {
        M = m;
    }

    public void setFilename(String filename) {
      API_Phrases.filename = filename;
    }

    public void setInput(InputStream input) {
        this.input = input;
    }
}
